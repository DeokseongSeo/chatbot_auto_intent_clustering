# import module
from utils import *
import re
import numpy as np
from operator import itemgetter
import json

class Compare_Intent:
    def __init__(self, t):
        # query = '무슨 색 좋아해'
        # 실제 sample 저장용 dictionary
        self.saved_dictionary = {}
        # clust 개수 세는 변수
        self.num_clust = 0
        # postagger
        self.t = t
        # tollerance
        self.tollerance = 0.6
        # stopwords
        self.stop_words = ['어떤', '무슨', '알다', '말', '하다', '되다',
                          '궁금하다', '가능하다', '시', '수', '인가요',
                          '있다', '하나요', '해야하나요', '좋다', '해', '요',
                          '한', '가요', '대해']


    def preprocess(self, str_list):
        str_list = list(map(lambda x: re.sub('[\?\.]', '', x), str_list))
        str_list = list(map(lambda x: re.sub('어떻게 해야 하나요', '', x), str_list))
        str_list = list(map(lambda x: re.sub('어떻게 되는 것인가요', '', x), str_list))
        str_list = list(map(lambda x: re.sub('어떻게 되나요', '', x), str_list))
        str_list = list(map(lambda x: re.sub('왜 그런가요', '', x), str_list))
        str_list = list(map(lambda x: re.sub('라고 나와요', '', x), str_list))

        str_pos = self.t.pos(str_list[0], stem=True)
        str_filt = np.array(str_pos)[np.where(np.in1d(list(map(itemgetter(1), str_pos)), ['Noun', 'Verb', 'Adjective', 'Alpha', 'Foreign', 'Number']))[0]]
        str_final = [' '.join(list(map(itemgetter(0), str_filt)))]

        split_str_list = list(map(lambda x: re.split(' ', x), str_final))
        filtered_word = list(map(lambda x: ' '.join(list(np.array(x)[np.logical_not(np.in1d(x, self.stop_words))])), split_str_list))
        return filtered_word[0]


    def _classify_intents(self, ind, query, tollerance=0.60, diehard=True):
        self.tollerance = tollerance

        if type(query) is not list:
            query = [query]
        processed_query = self.preprocess(query)

        # 첫 query 들어왔을 때 바로 저장하는 logic
        if self.num_clust == 0:
            self.num_clust += 1
            new_clust = 'clust_{}'.format(self.num_clust)
            self.saved_dictionary[new_clust] = {}
            self.saved_dictionary[new_clust]["items"] = []
            self.saved_dictionary[new_clust]["sentence"] = ''
            self.saved_dictionary[new_clust]["items"].append(query[0])
            self.saved_dictionary[new_clust]["sentence"] += processed_query

            print('{}_th query'.format(ind))
            return

        # 두번째 query 부터는 들어왔을 때 바로 새 클러스터 만들거나 기존 클러스터에 저장하는 로직
        query_jaso = jaso_split(processed_query)

        max_score_stack = []
        # 초반 클러스터에만 저장되는 문제를 해결하기 위해 뒤부터 탐색 적용
        keys = list(self.saved_dictionary.keys())[::-1]

        # 모든 클러스터에 대해 score 산출
        for key in keys:
            value = jaso_split(self.saved_dictionary[key]['sentence'])
            compare_score_output = jaso_compare(value, query_jaso, tollerance_word=self.tollerance)
            max_score_stack.append(np.mean(compare_score_output))

        # 가장 큰 score 갖는 것 부터 기준에 맞는지 파악하는 부분
        key_count = 0
        for index in np.argsort(max_score_stack)[::-1]:
            key_count += 1
            score = max_score_stack[index]

            if score == 0.0:
                self.num_clust += 1
                new_clust = 'clust_{}'.format(self.num_clust)
                self.saved_dictionary[new_clust] = {}
                self.saved_dictionary[new_clust]["items"] = []
                self.saved_dictionary[new_clust]["sentence"] = ''
                self.saved_dictionary[new_clust]["items"].append(query[0])
                self.saved_dictionary[new_clust]["sentence"] += processed_query

                print('{}_th query'.format(ind))
                return

            # 기존 클러스터에 저장
            if score >= self.tollerance:
                key = keys[index]
                self.saved_dictionary[key]['items'].append(query[0])
                tmp_query = ' ' + processed_query
                self.saved_dictionary[key]['sentence'] += tmp_query

                print('==={}_th query'.format(ind))
                return

            # 기준에 맞지 않으므로 for문을 계속 진행
            elif score < self.tollerance and key_count < self.num_clust:
                continue

            # 새 클러스터 만듦
            elif score < self.tollerance and key_count == self.num_clust:
                self.num_clust += 1
                new_clust = 'clust_{}'.format(self.num_clust)
                self.saved_dictionary[new_clust] = {}
                self.saved_dictionary[new_clust]["items"] = []
                self.saved_dictionary[new_clust]["sentence"] = ''
                self.saved_dictionary[new_clust]["items"].append(query[0])
                self.saved_dictionary[new_clust]["sentence"] += processed_query

                print('{}_th query'.format(ind))
                return


    def classify_intents(self, data, tollerance=0.60, diehard=True):
        for ind, query in enumerate(data):
            self._classify_intents(ind, query, tollerance, diehard)  # diehard: 보수적인 clustering


    def cluster_anealing(self, decay, iteration):
        for iter in range(iteration):

            print('새로운 iteration 시작 =========== {}'.format(iter))

            self.tollerance -= decay
            # 1개의 문서만 가진 cluster는 해제해서 새로운 데이터셋으로 구성
            length_info = np.array(list(map(lambda x: len(x[1]['items']), self.saved_dictionary.items())))
            just_one_string = np.where(length_info == 1)[0]
            next_dataset = []
            for ind in just_one_string:
                clust = 'clust_{}'.format(ind + 1)
                value = self.saved_dictionary[clust]['items'][0]
                # print(value)
                del self.saved_dictionary[clust]
                next_dataset.append(value)

            # cluster renumber
            tmp_dict = {}
            clust_num = 0
            for key, val in self.saved_dictionary.items():
                clust_num += 1
                clust_ = 'clust_{}'.format(clust_num)
                tmp_dict[clust_] = val

            self.num_clust = clust_num
            self.saved_dictionary = tmp_dict
            del tmp_dict

            next_dataset.sort(key=len, reverse=True)
            self.classify_intents(next_dataset, tollerance=self.tollerance, diehard=False)  # diehard: 보수적인 clustering

            print('\n\n\n\n\n\n\n\n\n\n')


    def save(self, path):
        with open(path, 'w', encoding='utf-8') as outfile:
            json.dump(self.saved_dictionary, outfile)

        print("saved to {}".format(path))


    def load(self, path):
        with open(path) as data_file:
            loaded_data = json.load(data_file)

        # 기존 json 불러오기
        self.saved_dictionary = loaded_data

        # 사람의 후처리로 items의 문장은 옮길테지만 sentence는 건들 수 없음
        # 변경된 items에 대한 sentence를 가지고 있어야 비교 가능하므로 새로 sentence 생성
        for ind, val in self.saved_dictionary.items():
            sentence = ''
            for i, query in enumerate(val['items']):
                tmp_query = ' ' + self.preprocess([query])
                sentence += tmp_query
            sentence = sentence.lstrip()
            self.saved_dictionary[ind]['sentence'] = sentence

        self.num_clust = len(loaded_data)

        self.tollerance = 0.60

        print("file loaded")

