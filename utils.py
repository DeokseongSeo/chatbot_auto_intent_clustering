# from hangul_utils import split_syllables #설치 : https://github.com/kaniblu/hangul-utils
import six
from difflib import SequenceMatcher
import re
import numpy as np
from collections import Counter



def __c(x):
    return six.unichr(x)

INITIALS = list(map(__c, [0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139,
                          0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147,
                          0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d,
                          0x314e]))

MEDIALS = list(map(__c, [0x314f, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154,
                         0x3155, 0x3156, 0x3157, 0x3158, 0x3159, 0x315a,
                         0x315b, 0x315c, 0x315d, 0x315e, 0x315f, 0x3160,
                         0x3161, 0x3162, 0x3163]))

FINALS = list(map(__c, [0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136,
                        0x3137, 0x3139, 0x313a, 0x313b, 0x313c, 0x313d,
                        0x313e, 0x313f, 0x3140, 0x3141, 0x3142, 0x3144,
                        0x3145, 0x3146, 0x3147, 0x3148, 0x314a, 0x314b,
                        0x314c, 0x314d, 0x314e]))

def check_syllable(x):
    return 0xAC00 <= ord(x) <= 0xD7A3

def split_syllable_char(x):
    """
    Splits a given korean character into components.
    :param x: A complete korean character.
    :return: A tuple of basic characters that constitutes the given characters.
    """
    if len(x) != 1:
        raise ValueError("Input string must have exactly one character.")

    if not check_syllable(x):
        raise ValueError(
            "Input string does not contain a valid Korean character.")

    diff = ord(x) - 0xAC00
    _m = diff % 28
    _d = (diff - _m) // 28

    initial_index = _d // 21
    medial_index = _d % 21
    final_index = _m

    if not final_index:
        result = (INITIALS[initial_index], MEDIALS[medial_index])
    else:
        result = (
            INITIALS[initial_index], MEDIALS[medial_index],
            FINALS[final_index - 1])

    return result

def split_syllables(string):
    """
    Splits a sequence of Korean syllables to produce a sequence of jamos.
    Irrelevant characters will be ignored.
    :param string: A unicode string.
    :return: A converted unicode string.
    """
    new_string = ""
    for c in string:
        if not check_syllable(c):
            new_c = c
        else:
            new_c = "".join(split_syllable_char(c))
        new_string += new_c

    return new_string


###############################################################
# jaso split function
###############################################################
#Jaso split function
def jaso_split(DOCUMENT):
    jamo = split_syllables(DOCUMENT)
    jamo = list(jamo.replace('  ', ' '))
    jamo = ''.join(jamo)
    return jamo


###############################################################
# jaso compare
###############################################################
def jaso_compare(reference, input, tollerance_word=0.6):
    in_words = re.split(' ', input)
    SCORES = []
    try:
        type(reference) is not list
    except:
        reference = list(reference)
        print('In query, reference type modified (from jaso_compare function)')
    ref_words = re.split(' ', reference)
    counter = Counter(ref_words)
    counter_sum = sum(counter.values()) + len(counter.values())
    ref_score = np.array(compare_core(ref_words, in_words)) > tollerance_word

    sentence_score = 0
    for word in np.array(in_words)[ref_score]:
        word_weight = (counter.get(word, 0)+1) / counter_sum # Additive smoothing
        sentence_score += word_weight

    SCORES.append(sentence_score)

    return SCORES

def compare_core(ref_words, in_words):
    input_words_score = []
    for in_word in in_words:
        set_score = 0

        for ref_word in ref_words:
            score = similar(ref_word, in_word)
            set_score = max(set_score, score)

        input_words_score.append(set_score)

    return input_words_score

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

