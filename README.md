# Chatbot_Auto_intent_clustering

- 작성자: 서덕성 선임, Aibril unit
- 작성일: 2018-09-18

- part2에서 유정상 선임, 신은지 선임과 함께 진행한 intent 자동 클러스터링 코드입니다.
- 자음, 모음 단위 접근을 하며, 클러스터 내 단어 출현 빈도를 이용한 weight를 주어 클러스터링을 합니다.
- 초기 클러스터링 진행 후 anealing을 통해 담금질 작업도 진행됩니다.

- 데이터는 단순히 text만 넣으시면 됩니다.
	ex)['에이브릴 프로젝트 생성 어떻게 하나요?',
	    '프로젝트 생성 방법 알려주세요',
		...]

- intent_clustering_main.py를 통해 사용하실 수 있습니다.
- 클러스터링 방법은 CompareIntent.py를 참고하시면 확인하실 수 있습니다.
- training data에 대해 먼저 클러스터링 하고, json을 받으신 뒤 사람이 후처리 하고, 이를 다시 load하는 부분까지 완성되어 있습니다.

- 데이터가 많을 수록 속도 관련 이슈가 발생할 수 있습니다.
	ex) 2200개 위 예제 정도 길이의 text를 가진 corpus의 경우 약 39분 소요 (samsung notebook에서)
