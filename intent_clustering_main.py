#####################################################################################################
# import modules
#####################################################################################################
import numpy as np
import pandas as pd
from CompareIntent import Compare_Intent
import datetime
from konlpy.tag import Twitter
t = Twitter()


#####################################################################################################
# data set load
#####################################################################################################
data = pd.read_csv('./AIA.txt', delimiter='\t', encoding='utf-8', header=None)
data = list(np.array(data.iloc[:, 0]))
data.sort(key=len, reverse=False) # sorts by descending length
tr_data = data[:2200]
ts_data = data[2200:]
tollerance = 0.60
decay = 0.02
iteration = 10
dataset = 'AIA'
filename = '{}_tollerance{}_decay{}'.format(dataset, tollerance, decay)
save_path = './{}.json'.format(filename)


#####################################################################################################
# Do clustering
#####################################################################################################
# 첫 객체 생성
CI = Compare_Intent(t)

# start time
start = datetime.datetime.today()
# train data에 대한 clustering
CI.classify_intents(tr_data, tollerance=tollerance, diehard=True)  # diehard: 보수적인 clustering
# cluster anealing
CI.cluster_anealing(decay=decay, iteration=iteration)
# save json file -> 사람의 후처리를 위한 json 파일
CI.save(save_path)
# end time
end = datetime.datetime.today()

print('소요시간: {}'.format(end-start))


#####################################################################################################
# test clustering
#####################################################################################################
# 새 객체 생성
CI2 = Compare_Intent(t)
# 후처리 완료된 json 읽어 오기
CI2.load(save_path)
# test data에 대한 clustering
CI2.classify_intents(ts_data, tollerance=tollerance, diehard=True)
# cluster anealing
CI2.cluster_anealing(decay=decay, iteration=iteration)
# check output
CI.saved_dictionary


# word_freq = np.array([3, 3, 2, 1])
# word_weight1 = (word_freq+0) / sum(word_freq)
# word_weight2 = (word_freq+1) / (sum(word_freq)+len(word_freq)) #additive smoothing